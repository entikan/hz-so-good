import sys

from panda3d.core import CardMaker
from panda3d.core import LineSegs
from panda3d.core import loadPrcFile

from direct.showbase.ShowBase import ShowBase
from direct.showbase.Transitions import Transitions

from keybindings.device_listener import add_device_listener
from keybindings.device_listener import SinglePlayerAssigner

from game import Game
from game.sequenceplayer import SequencePlayer

from game.gui import HUD


loadPrcFile("config.prc")
base = ShowBase()
add_device_listener(assigner=SinglePlayerAssigner())
base.win.set_clear_color((0,0,0,1))
base.turn_speed = 0.35
base.card_maker= CardMaker('card')
base.card_maker.set_frame(-0.5,0.5,0,1)
base.line_segs = LineSegs('lines')
base.line_segs.set_thickness(2)
base.transition = Transitions(loader)
base.sequence_player = SequencePlayer()
base.focus = []
base.game = Game()
base.run()