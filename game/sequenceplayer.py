from direct.interval.IntervalGlobal import Sequence
from direct.interval.IntervalGlobal import Parallel
from direct.interval.IntervalGlobal import Func
from direct.interval.IntervalGlobal import Wait


class SequencePlayer():
    def __init__(self):
        self.wait = base.turn_speed
        self.parallel = None

    def end(self):
        self.wait = base.turn_speed
        self.parallel = None

    def hold(self, time):
        self.wait = time
        self.add_to_sequence()

    def add_to_sequence(self, *kwargs):
        if not self.parallel:
            self.parallel = Parallel()
        for item in kwargs:
            self.parallel.append(item)

    def finalize(self):
        if not self.parallel:
            self.parallel = Parallel()
        func = Sequence(Wait(self.wait), Func(self.end))
        self.parallel.append(func)
        self.parallel.start()