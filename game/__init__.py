from .player import Player
from .maps import Map
from .gui import *
from .events import ZapFade, Warning


class Game():
    def __init__(self):
        self.sfx = {}
        for name in [
            'buzz','die','download','intro','mi','scan','connect',
            'door','gameover','maj','re','shred','data-read','do','uwon',
            'hurt','min','sad','spoof', 'tick', 'sell', 'buy', 'scream'
        ]:
            self.sfx[name] = loader.load_sfx('assets/sfx/{}.wav'.format(name))

        self.currently_playing_song = None
        self.songs = {}
        for name in list('abcde')+['home']:
            self.songs[name] = loader.load_sfx('assets/songs/{}.ogg'.format(name))
            self.songs[name].set_loop(True)
            self.songs[name].set_volume(0.3)

        self.maps = {}
        for b, bookmark in enumerate([
            'hack.me', 'hellotoo.yu', 'yarhee.co',
            'exclusiv.vv', 'foodscoot.toot', 'buypls.me',
            'myeducation.edu', 'insureforgoo.dl', 'health.md',
            'coin.4u', 'police.whoop', 'coorporation.co',
            'state.gov', 'state_secret.gov', 'terrorist.org',
            'fbicsiciacisncis.gov', 'theend.com',
        ]):
            self.maps[bookmark] = Map(bookmark, b%5)

        base.camLens.set_fov(130)
        base.camLens.set_near(0.1)
        base.camLens.set_far(64)
        self.focus = []
        self.event = None
        self.player = Player()
        self.current_map = None
        self.start = True
        self.starting = True
        self.end = False
        base.task_mgr.add(self.menu)

    def menu(self, task):
        if self.end:
            return
        if self.start:
            if self.starting:
                Warning()
                self.starting = False

        else:
            context = base.device_listener.read_context('player')
            if len(base.focus) == 0 and not self.event:
                if self.current_map:
                    if context['yes']:
                        comp()
                elif context['yes']:
                    main()
        return task.cont

    def disconnect(self):
        ZapFade('Disconnecting...')
        self.player.stats.bookmarks[self.current_map.name] = self.current_map
        self.current_map.root.detach_node()
        self.current_map = None
        self.player.root.detach_node()
        self.player.stats.userlevel = 'owner'
        base.game.sfx['connect'].play()
        base.game.play_song('home')

    def play_song(self, song):
        time = 0
        if self.currently_playing_song:
            time = self.songs[self.currently_playing_song].get_time()
            self.songs[self.currently_playing_song].stop()
        self.songs[song].set_time(time)
        self.songs[song].set_loop()
        self.songs[song].play()
        self.currently_playing_song = song

    def stop_all_music(self):
        for song in self.songs:
            self.songs[song].stop()
        self.currently_playing_song = None

    def zap(self, txt):
        ZapFade(txt)
