import sys
from random import randint
from panda3d.core import SequenceNode, NodePath, TextureStage
from .gui import *
from .warez import Data
from .shaders import SHADER
from .anim import *

ENDPRICE = 2000000

class Stats():
    def __init__(self):
        self.name = 'player'
        self.money = 0
        self.traced = [10,10]
        self.cloaked = [0,10]
        self.integrity = [5,5]
        self.userlevel = 'owner'
        self.bookmarks = {}
        self.warez = []
        self.data = []
        self.drive = [0,20]
        self.drive_update_cost = 20
        self.level = 1

    def end_turn(self):
        o = 0
        if self.userlevel == 'admin':
            o = (base.game.current_map.difficulty+1)*2
        elif self.userlevel == 'user':
            o = base.game.current_map.difficulty+1
        if self.cloaked[0] > 0:
            self.cloaked[0] -= o
        elif self.traced[0] > 0:
            self.traced[0] -= o
        else:
            Enemy(base.game.current_map.difficulty)
            base.game.sfx['sad'].play()

    def hurt(self, amount):
        self.integrity[0] -= amount
        if self.integrity[0] <= 0:
            base.game.disconnect()
            for f in base.game.focus:
                f.stop()
            if base.game.event: base.game.event.end()
            base.game.zap('integrity integral. shutting downde3i109 48###103875 138709#####13 701####### #.')
            base.accept('escape', sys.exit)
            base.game.sfx['gameover'].play()
        else:
            base.game.sfx['hurt'].play()
            Message('Integrity -'+str(amount))

    def sell(self):
        if len(self.data):
            items = []
            for data in self.data:
                items.append(('{}:{} COINS'.format(data.name, data.value),data.sell, self))
            Menu(items)
        else:
            base.game.sfx['min'].play()
            Message('No data to sell.')

    def upgrade_drive(self):
        if self.money >= int(self.drive[1]**2):
            self.money -= int(self.drive[1]**2)
            self.drive[1] *= 1.6
            self.drive[1] = int(self.drive[1])
            Message('HDD is now upgraded! Room to brreeeaathhhhhe!')
            base.game.sfx['buy'].play()
        else:
            Message('Not enough coin, friend.')
            base.game.sfx['sad'].play()

    def upgrade_integrity(self):
        if self.money >= int(self.integrity[1]**2):
            self.money -= int(self.integrity[1]**2)
            self.integrity[1] *= 1.6
            self.integrity[1] = int(self.integrity[1])
            Message('Integrity upgraded! Go gettem tiger.')
            base.game.sfx['buy'].play()
        else:
            Message('Not enough coin, friend.')
            base.game.sfx['sad'].play()

    def recloak(self):
        if self.money >= self.integrity[1]*4:
            self.money -= int(self.integrity[1]*4)
            self.cloaked[0] = self.integrity[1]
            Message('Cloak the pleasure, cloak the fun. You have the cloak power activated.')
            base.game.sfx['buy'].play()
        else:
            Message('Not enough coin, friend.')
            base.game.sfx['sad'].play()

    def reset_integrity(self):
        p = int((self.integrity[1]-self.integrity[0])*2)
        if self.money >= p:
            self.money -= p
            self.integrity[0] = self.integrity[1]
            base.game.sfx['buy'].play()
            Message('Integrity reset and done over again. Stay safe.')

        else:
            Message('Not enough coin, friend.')
            base.game.sfx['sad'].play()

    def win(self):
        if self.money >= ENDPRICE:
            base.game.end = True
            base.game.player.root.detach_node()
            Message('You blew up the internet. Good job. You won!', Win)
        else:
            Message('Not enough coin to win. Players with a lot of coin win, meaning you didnt win.')
            base.game.sfx['sad'].play()

    def buy(self):
        items = [
            ('HDD++: {}C'.format(int(self.drive[1]**2)), self.upgrade_drive),
            ('Integrity++: {}C'.format(int(self.integrity[1]**2)), self.upgrade_integrity)
        ]
        if not self.cloaked[0] == self.integrity[1]:
            items.append(('Recloak: {}C'.format(self.integrity[1]*4), self.recloak))
        if not self.integrity[0] == self.integrity[1]:
            items.append(('Reset integrity: {}C'.format((self.integrity[1]-self.integrity[0])*2), self.reset_integrity))
        for warez in self.warez:
            items.append(('{} V{}: {}C'.format(warez.name, warez.version+1, int(warez.get_upgrade_cost())), warez.buy, self))
        items.append(('Win game: {}C'.format(ENDPRICE),self.win))
        items.append(('nvm', none))
        Menu(items, title='buy warez!')

    def get_bookmark(self):
        all_visited = True
        for m in self.bookmarks:
            if not self.bookmarks[m].visited:
                all_visited = False
                break
        if not all_visited:
            Message('No new bookmarks available. Come back later.')
        else:
            for m in base.game.maps:
                if m not in self.bookmarks:
                    self.bookmarks[m] = base.game.maps[m]
                    Message('New bookmark added. Good hacking!')
                    return

    def check_data(self):
        if len(self.data):
            items = []
            for data in self.data:
                items.append((data.name, data.display))
            items.append(('nvm', none))
            Menu(items)
        else:
            Message("""
    You don't have any data.
            """)

    def check_bookmarks(self):
        items = []
        for bookmark in self.bookmarks.values():
            items.append((bookmark.name, bookmark.connect, base.game.player))
        items.append(('nvm', none))
        Menu(items, title='bookmarks')

    def run_warez(self, who, along=None):
        warez = []
        for ware in self.warez:
            if ware.version > 0:
                warez.append((ware.name+'_v'+str(ware.version)+'.-', ware.use, self, who, along))
        warez.append(('nvm', none))
        Menu(warez)


class Event():
    def __init__(self):
        self.root = render2d.attach_new_node('event')
        self.root.set_shader(SHADER)
        self.root.set_shader_input('speed', 0.01)
        self.root.set_depth_write(False)
        self.root.set_bin('background',-100)
        base.game.event = self

    def end(self):
        self.root.detach_node()
        base.game.event = None

    def turn(self):
        self.end()


class ZapFade(Event):
    def __init__(self, text, callback=None):
        super().__init__()
        self.callback = callback
        self.stats = Stats()
        card = animated_texture(
            NodePath(base.card_maker.generate()),
            loader.load_texture('assets/zapfade.png',minfilter=0, magfilter=0)
            ,4,4
        )
        card.node().set_frame_rate(50)
        card.reparent_to(self.root)
        card.set_z(-1)
        card.set_scale(2)
        self.frame = 0
        self.message = Message(text, callback=callback)

    def turn(self):
        self.frame += 1
        if self.frame >= 32:
            self.end()

class Win(Event):
    def __init__(self):
        super().__init__()
        base.accept('escape', sys.exit)
        base.game.sfx['uwon'].play()
        card = animated_texture(
            NodePath(base.card_maker.generate()),
            loader.load_texture('assets/uwon.png',minfilter=0, magfilter=0)
            ,2,2
        )
        card.node().set_frame_rate(10)
        card.reparent_to(self.root)
        card.set_z(-1)
        card.set_scale(2)

    def turn(self):
        pass


class OneShotAnimation(Event):
    def __init__(self, image, col=2, row=2, framerate=15, callback=None):
        super().__init__()
        self.stats = Stats()
        card = animated_texture(
            NodePath(base.card_maker.generate()),
            loader.load_texture(image ,minfilter=0, magfilter=0) ,col,row
        )
        card.node().set_frame_rate(framerate)
        card.reparent_to(self.root)
        card.set_z(-1)
        card.node().play()
        card.set_scale(2)
        self.anim = card.node()
        self.callback = callback

    def turn(self):
        if not self.anim.is_playing():
            base.game.start = base.game.starting = False
            self.end()
            if self.callback:
                self.callback()

class Warning(Event):
    def __init__(self):
        super().__init__()
        self.stats = Stats()
        card = animated_texture(
            NodePath(base.card_maker.generate()),
            loader.load_texture('assets/warning.png',minfilter=0, magfilter=0)
            ,1,1
        )
        card.reparent_to(self.root)
        card.set_z(-1)
        card.set_scale(2)
        Message('By pressing Z to continue, you are agreeing that you are willing play this game at your own risk.')
        self.frame = 0
        base.game.play_song('home')

    def turn(self):
        self.frame += 1
        if self.frame >= 32:
            self.end()
            OneShotAnimation('assets/title.png', framerate=1, callback=main)


class Enemy(Event):
    def __init__(self, level=0):
        super().__init__()
        base.game.play_song('e')
        self.is_enemy = True
        self.names = ['DataNanny','Skriptkid','HyperWoyem', 'StilletoAccessManager', 'Armazornian', 'SecuSquirrell', 'RawkBaws']
        self.level = level
        if randint(0,1):
            self.level += randint(0,1)
        self.damage = self.level+1
        self.level = self.level%len(self.names)
        hp = int((level+1)**1.5)
        self.health = [hp, hp]
        self.version = int(self.health[1]//(self.level+1))+1
        self.scanned = False
        self.uncard = animated_texture(
            NodePath(base.card_maker.generate()),
            loader.load_texture('assets/enemy_unknown.png',minfilter=0, magfilter=0)
            ,2,2,
        )
        self.uncard.reparent_to(self.root)
        self.uncard.set_z(-0.5)

        self.realcard = animated_texture(
            NodePath(base.card_maker.generate()),
            loader.load_texture('assets/enemies/{}.png'.format(self.level),minfilter=0, magfilter=0)
            ,2,2,
        )
        self.realcard.reparent_to(self.root)
        self.realcard.set_z(-0.5)
        self.realcard.hide()

        self.turn()
        Message("You've been tracked. Security was notified.")

    def go_dead(self):
        self.end()
        base.game.sfx['scream'].play()

    def attack(self, amount):
        self.health[0] -= amount
        if self.health[0] <= 0:
            base.game.player.stats.traced[0] = base.game.player.stats.traced[1]
            Message(self.names[self.level]+' crashed, traceback reinitialized.', self.go_dead)
            Message(str(amount)+' security integrality diminished')
        else:
            Message(str(amount)+' security integrality diminished',self.do_attack)

    def do_attack(self):
        if self.health[0] <= 0:
            return
        else:
            base.game.player.stats.hurt(self.damage)
            if self.scanned:
                Message(self.names[self.level]+' sends malicous packet...')
            else:
                Message('Security sends malicous packet...')

    def display(self, along=None):
        self.scanned = True
        self.uncard.hide()
        self.realcard.show()
        Message(self.names[self.level]+'!!!\nVersion:{}.00\nHP:{}/{}'.format(self.level, *self.health), along)

    def disconnect(self):
        self.end()
        base.game.disconnect()

    def turn(self):
        if base.game.player.stats.integrity[0] > 0 and self.health[0] > 0:
            Menu((
                ('warez', base.game.player.stats.run_warez, self, self.do_attack),
                ('disconnect', self.disconnect),
            ))
        else:
            self.end()

class HandleData(Event):
    def __init__(self, data):
        super().__init__()
        self.data = data
        card = animated_texture(
            NodePath(base.card_maker.generate()),
            loader.load_texture('assets/data.png',minfilter=0, magfilter=0)
            ,2,2,
        )
        card.reparent_to(self.root)
        card.set_z(-0.5)
        self.turn()
        Message('Theres data here...')

    def leave(self):
        base.game.player.retrace()
        self.end()

    def turn(self):
        player = base.game.player
        Menu((
            ('warez', player.stats.run_warez, self.data),
            ('leave', self.leave),
        ))
