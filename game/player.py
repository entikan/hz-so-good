from panda3d.core import NodePath, Vec2
from .gui import Menu, comp, HUD
from .events import Stats, Enemy, HandleData
from .warez import *

def round_vec(vec):
    for v, value in enumerate(vec):
        vec[v] = round(value)
    return vec


class Player():
    def __init__(self):
        self.root = NodePath('player')
        self.in_menu = False
        self.force_forwards = False
        base.task_mgr.add(self.update)
        base.cam.reparent_to(self.root)
        base.cam.set_z(0.4)
        self.stats = Stats()

        self.stats.warez.append(Shred())
        self.stats.warez.append(Scanner())
        self.stats.warez.append(Download())
        self.stats.warez.append(Spoof())
        self.stats.warez[2].version = 1
        self.enemy_time = 0
        HUD(self.stats)

    def move(self, force=None):
        context = base.device_listener.read_context('player')
        if force:
            context['move'].y = force

        interval = None
        hpr = round_vec(self.root.get_hpr())
        pos = round_vec(self.root.get_pos())
        if context['move'].x > 0:
            self.root.set_h(self.root, -90)
            base.game.sfx['mi'].play()
            interval = self.root.quatInterval(base.turn_speed, self.root.get_hpr(), startHpr=hpr, blendType='easeOut')
        elif context['move'].x < 0:
            self.root.set_h(self.root, 90)
            interval = self.root.quatInterval(base.turn_speed, self.root.get_hpr(), startHpr=hpr, blendType='easeOut')
            base.game.sfx['mi'].play()
        elif context['move'].y < 0:
            self.root.set_h(self.root, 180)
            interval = self.root.quatInterval(base.turn_speed, self.root.get_hpr(), startHpr=hpr, blendType='easeOut')
            base.game.sfx['mi'].play()
        elif context['move'].y > 0:
            a = int((-hpr[0]%360)//90)
            d = ['n','e','s','w'][a]
            door = None
            if d in self.tile.neighbours:
                if d in self.tile.doors:
                    door = self.tile.doors[d]
                    level = ['guest','user','admin']
                    if level.index(self.stats.userlevel) >= level.index(door.userlevel):
                        if not door.is_open:
                            door.open()
                            base.game.sfx['door'].play()
                            self.tile = self.tile.neighbours[d]
                            self.force_forwards = True
                            return
                    else:
                        Message('You need to be at least {} level to enter.'.format(door.userlevel))
                        return
                self.stats.end_turn()
                self.root.set_y(self.root, 1)
                base.game.sfx['re'].play()
                self.tile = self.tile.neighbours[d]
                interval = self.root.posInterval(base.turn_speed, self.root.get_pos(), startPos=pos)
        if interval:
            base.sequence_player.add_to_sequence(interval)
            base.sequence_player.finalize()

    def retrace(self):
        d = ['s','w','n','e'][int((-self.root.get_h()%360)//90)]
        pos = self.root.get_pos()
        self.root.set_y(self.root, -1)
        if d in self.tile.neighbours:
            self.tile = self.tile.neighbours[d]
        else:
            print(d, self.tile.neighbours, self.tile.doors)

        interval = self.root.posInterval(base.turn_speed, self.root.get_pos(), startPos=pos)
        base.sequence_player.add_to_sequence(interval)
        base.sequence_player.finalize()

    def update(self, task):
        self.stats.money = int(self.stats.money)
        if base.sequence_player.parallel or len(base.focus) > 0:
            return task.cont
        if base.game.event:
            base.game.event.turn()
            return task.cont
        if not base.game.current_map:
            return task.cont
        pos = round_vec(self.root.get_pos())
        if self.force_forwards:
            base.game.sfx['re'].play()
            self.force_forwards = False
            pos = self.root.get_pos()
            self.root.set_y(self.root, 1)
            interval = self.root.posInterval(base.turn_speed, self.root.get_pos(), startPos=pos)
            base.sequence_player.add_to_sequence(interval)
            base.sequence_player.finalize()
            #self.stats.end_turn()
            if self.tile.data:
                HandleData(self.tile.data)
            return task.cont
        self.move()
        return task.cont
