import sys
from random import choice
from panda3d.core import NodePath
from panda3d.core import TextNode
from .shaders import SHADER


class GUI():
    def __init__(self, name, callback=None):
        self.parts = loader.load_model('assets/gui.bam')
        self.root = NodePath(name)
        self.root.set_shader(SHADER)
        self.root.set_shader_input('speed',0.005)
        self.callback = callback
        self.root.reparent_to(render2d)
        base.task_mgr.add(self.update)
        self.stopped = False
        try:
            context = base.device_listener.read_context('menu')
            context['yes'] = False
        except:
            pass
        base.focus.append(self)

    def stop(self):
        self.stopped = True

    def do(self):
        pass

    def update(self, task):
        if base.focus[-1] == self:
            self.root.show()
            if self.stopped or not self.do():
                self.root.detach_node()
                if self.callback: self.callback()
                base.focus.remove(self)
                return task.done
        else:
            self.root.hide()
        return task.cont


class Message(GUI):
    def __init__(self, text, callback=None):
        super().__init__('message', callback)
        background = self.parts.find('message')
        background.reparent_to(self.root)
        background.clear_transform()
        background.set_scale(0.7)
        self.textnp = self.root.attach_new_node(TextNode('message'))
        self.textnp.node().set_align(2)
        self.textnp.node().set_wordwrap(26)
        self.text = list(text)
        self.textnp.set_z(-0.2)
        self.root.set_scale(0.06)
        self.root.set_pos(0,0,-0.45)

    def do(self):
        context = base.device_listener.read_context('menu')
        if not len(self.text) == 0:
            c = self.text.pop(0)
            base.game.sfx['tick'].play()
            self.textnp.node().text += c
            if context['yes']:
                self.textnp.node().text += ''.join(self.text)
                self.text = []
        else:
            if context['yes']:
                base.game.sfx['do'].play()
                return False
        return True

class HUD(GUI):
    def __init__(self, stats):
        super().__init__('hud')
        self.items = []
        self.stats = stats

        portraits = ['happy','sad','evil','dead']
        self.portraits = {}
        for name in portraits:
            portrait = self.parts.find('portrait_'+name).copy_to(self.root)
            portrait.hide()
            portrait.clear_transform()
            portrait.set_pos(-5.3,0,2)
            self.portraits[name] = portrait
        self.set_portrait('happy')

        self.add_item('')
        self.add_item('')
        self.add_item('')
        self.add_item('')
        self.add_item('')

        self.root.set_scale(0.05)
        self.root.set_pos(1,0,0.2)
        self.root.set_depth_write(False)
        self.root.set_bin('background',-1000)

        base.focus.remove(self)

    def set_portrait(self, name):
        for p in self.portraits:
            if p == name:
                self.portraits[p].show()
            else:
                self.portraits[p].hide()

    def update(self, task):
        self.items[0].node().text = 'INTEGRITY: {}/{}'.format(*self.stats.integrity)
        if self.stats.integrity[0] <= 0:
            for i in range(1,5):
                r = ""
                for x in range(14):
                    r += choice(list('xqxqrusklxtz6qrmbp@!&^%$#'))
                self.items[i].node().text = r
            return task.cont

        self.items[1].node().text = 'HDD: {}/{}'.format(*self.stats.drive)
        self.items[2].node().text = 'COIN: '+str(self.stats.money)

        if not self.stats.userlevel == "owner":
            self.items[3].parent.show()
            self.items[4].parent.show()
            self.items[3].node().text = 'ACCESS: '+str(self.stats.userlevel).upper()
            if self.stats.cloaked[0] > 0:
                self.items[4].node().text = 'CLOAK: {}'.format(self.stats.cloaked[0])
            else:
                self.items[4].node().text = 'TRACEBACK: {}/{}'.format(*self.stats.traced)
        else:
            self.items[3].parent.hide()
            self.items[4].parent.hide()

        if self.stats.integrity[0] <= self.stats.integrity[1]//4:
            self.set_portrait('dead')
        elif self.stats.integrity[0] <= self.stats.integrity[1]//2:
            self.set_portrait('sad')
        elif self.stats.userlevel == 'admin':
            self.set_portrait('evil')
        else:
            self.set_portrait('happy')

        return task.cont

    def add_item(self, text, shape='poker'):
        bg = self.parts.find(shape).copy_to(self.root)
        txt = bg.attach_new_node(TextNode('menu_item'))
        txt.set_pos(-1.5,0,-2.1)
        txt.node().align = 4
        txt.node().text = text
        txt.set_scale(0.75)
        bg.set_z(1.1-((len(self.items)+4)*2.7))
        self.items.append(txt)


class Menu(GUI):
    def __init__(self, items=[], title='menu', sendalong=None):
        super().__init__('menu_'+title)
        self.sendalong = None
        self.sel = 0
        self.items = items
        self.items_nps = []

        bg = self.parts.find('box').copy_to(self.root)
        txt = bg.attach_new_node(TextNode('menu_title'))
        txt.set_pos(1,0,-2)
        txt.node().text = title
        bg.set_z(0.5)
        bg.set_x(-0.95)
        bg.set_depth_write(False)
        bg.set_bin('background',-1)
        for i, item in enumerate(items):
            bg = self.parts.find('item').copy_to(self.root)
            txt = bg.attach_new_node(TextNode('menu_item'))
            txt.set_pos(1,0,-2)
            txt.node().text = item[0]
            txt.set_scale(0.75)
            if len(item[0]) > 20:
                txt.set_scale(0.5)

            bg.set_z(1.1-((i+1)*2.7))
            self.items_nps.append(bg)
        self.root.reparent_to(render2d)
        self.root.set_scale(0.05)
        self.root.set_pos(-0.9,0,0.9)
        context = base.device_listener.read_context('menu')
        context['yes'] = False
        base.game.sfx['maj'].play()

    def do(self):
        self.items_nps[self.sel].set_x(0)
        context = base.device_listener.read_context('menu')
        if context['up']:
            self.sel -= 1
            base.game.sfx['do'].play()
        elif context['down']:
            self.sel += 1
            base.game.sfx['do'].play()
        elif context['yes']:
            base.game.sfx['maj'].play()
            selected = self.items[self.sel]
            selected[1](*selected[2:])
            return False
        self.sel %= len(self.items)
        self.items_nps[self.sel].set_x(2)
        return True


def none():
    pass

def options():
    pass

def confirm_quit():
    Menu((
        ('yes', sys.exit),
        ('nvm', none)
    ), title='quit?')

def meatspace():
    def credits():
        Message("""Thanks to:
zooperdan,simulan, virtualpierogi, rdb, nica, fireclaw, moguri...
 and special thanks to the http:/www.dungeoncrawlers.com community!
""")
        Message("""Design by Entikan & Schwarzbaer
Code by Entikan
Textures by Tael & Entikan
Characters by Sorny
Music/SFX by Entikan
""")
        Message("""HZ SO GOOD. 2022.

Created in 1 week for the 2022 Dungeon Crawl Jam.
Paid for by The Ergot Initiative.
Created by Entisoft.
""")

    Menu((
        ('quit game',confirm_quit),
        ('credits',credits),
        ('nvm', none),
    ), title='meatspace')

def comp():
    player = base.game.player
    Menu((
      ('warez',player.stats.run_warez, player.tile),
      ('data',player.stats.check_data),
      ('disconnect',base.game.disconnect),
      ('meatspace',meatspace),
      ('nvm', none),
    ), title='comp')

def shop():
    player = base.game.player
    Menu((
      ('sell data', player.stats.sell),
      ('buy warez',player.stats.buy),
      ('get bookmark', player.stats.get_bookmark),
      ('nvm',main),
    ), title='amber road')

def main():
    player = base.game.player
    if player.stats.integrity[0] <= 0:
        items = []
        for i in range(8):
            r = ""
            for i in range(14):
                r += choice(list('xqxqrusklxtz6qrmbp@!&^%$#'))
            items.append((r, none))
        Menu(items, title=r)
    else:
        Menu((
          ('amber road', shop),
          ('connect',player.stats.check_bookmarks),
          ('warez',player.stats.run_warez, player),
          ('data',player.stats.check_data),
          ('meatspace',meatspace),
        ), title='main')

