from random import randint, choice
from .gui import Message, Menu, none

data = {
    'guest' : [
        ('Free Sample!', 'Have fun with your free sample!', 8, 80),
        ('INDEX.HTML', 'WELCOME!', 16, 80),
        ('spam', 'did you ever try to be dray in the wafer? buy now!', 10, 62),
        ('gooble', 'this is terrible news. everybody stay indoors', 8, 52),
        ('news journal', 'this is the news. it was a day and everyone had a day again.', 20, 120),
        ('recipe', '1 food item\nadd 1 food item and combine with food item #153113. enjoy.', 10, 100),
        ('opinion', 'I have strong opinions that nobody likes to hear so I say them louder.', 20, 200),
        ('revolution!', 'Are you unhappy? Its time to begin the revolution.', 10, 80),
        ('coin4u', 'want free coin? me too. you can get them by selling data', 12, 300),
        ('coin4u', 'sellemdism8', 20, 400),
        ('help me!', 'help me! I am stuck in the top of the tower!', 11, 111),
        ('fantastic', 'this is exactly what I mean when I say "chateau"', 15, 120),
        ('funny chat log', 'example: hello?', 15, 107),
        ('ad', 'Do you want eyebeam? Get eyebeam now! 25% off the charts!', 14, 105),
        ('ad', 'Which comes first? 64 or 8? Try our IQ test!', 14, 89),
        ('ad', 'Tomatoes. Get your tomatoes here. Delicious tomatoes.', 12, 160),
        ('ad', 'Ugly? Fix it. Pills!', 19, 92),
        ('spam', 'fart fart fart fart', 19, 95),
        ('spam', 'gyurgayurgayurgayurgayurgayurgayurgayurgayurgayur', 19, 98),
        ('spam', 'ANIMEL? ANIMAL DUH SPELLING!', 19, 109),
        ('first', 'first: first!', 19, 102),
    ],
    'user' : [
        ('your account', 'your accound has been enabled. thank you very much', 110, 500),
        ('your account', 'wlecome. your accound is enabled again sorry for the mixup', 120, 500),
        ('your groceries','your groceries will arive within the next 24 hours', 150, 500),
        ('nap berry!', 'heeeeerrrreeeeeeeees your nap berry!', 100, 500),
        ('rabbits foot', 'sometimes you are lucky and sometimes not', 120, 520),
        ('members only', 'hello member! this is the reciever antilope seven.', 100, 500),
        ('members only', 'hello member! what about? True true.', 150, 400),
        ('members only', 'here is your monthly coupon', 140, 500),
        ('members only', 'aaooouuwwww good luck!', 150, 450),
        ('members only', 'here is your shnarp. I hope you have fun widdit!', 150, 600),
        ('members only', 'here is your vacation-time overhours. I hope you have fun widdit!', 150, 500),
        ('email', 'no worries it is ok thx.', 150, 500),
        ('email', 'tres bien. viola d`amargand de supermarche oizo 777', 150, 500),
        ('email', 'she is after me and out to kill me help!', 250, 500),
        ('email', 'turns out it was a haunted house ddooiiiiii', 150, 600),
        ('credit sequence', 'credit sequence! envelope this data to support yourself!', 110,2000),
        ('flamboyant sequence', 'flamboyand sequence! envelope this data to support yourself!', 150,800),
        ('cream', 'cream? cream!', 100,900),
    ],
    'admin': [
        ('late', 'youre late again wtf -management', 500, 7000),
        ('log out plz', 'please log out next time ok -management', 500, 6000),
        ('razer', 'you left the razer on please turn it off next time -management', 500, 8000),
        ('email', 'the passcode is 7745128 respond', 500, 3000),
        ('email', 'the passcode is 3r98139 respond', 500, 4000),
        ('email', 'incoming email. will not be here for vacation.', 500, 5000),
        ('email', 'were supplying the nemey.', 500, 3000),
        ('email', 'if you read this im already dead.', 500, 2000),
        ('void', '.....!.....!..!.....!....!.!.!...!', 111, 3111),
        ('ballok', 'ballok gave me his phone number unbelievable! scared me', 500,3000),
        ('ballok', 'ballok was waiting for me in the park unbelievable! scared me', 500,5000),
        ('ballok', 'ballok told me he was horny! scared me', 500,10000),
        ('pm', 'I crave ice cream. Love you baby', 500, 10000),
        ('pm', 'What are you wearing?', 500, 10000),
        ('pm', 'here is a state secretes dont open or read while connected', 500, 10000),
        ('pm', 'if you dont come back right now you are fired.', 500, 10000),
        ('pm', 'thats ok i kind of like it weird', 500, 10000),
        ('pm', 'yes sure why not ok?', 500, 10000),
        ('pm', 'yes you keep the money i cant use it atm', 500, 10000),
        ('pm', 'this is a very secret meeting all across the board', 500, 10000),
        ('pm', 'they have street legal war vehicles there', 500, 10000),
        ('void', 'arrested again?', 111, 11110),
    ]
}



class Data():
    def __init__(self, userlevel='guest'):
        i = choice(data[userlevel])
        self.size = i[2]
        self.value = i[3]*3
        self.name = i[0]
        self.content = i[1]

    def sell(self, who):
        who.data.remove(self)
        who.drive[0] -= self.size
        who.money += self.value
        base.game.sfx['sell'].play()

    def display(self):
        base.game.sfx['data-read'].play()
        Message("""<size: {}>  <value: {} coin>
{}
{}
        """.format(self.size, self.value, self.name.upper(), self.content))


class Warez():
    def __init__(self, name):
        self.name = name
        self.version = 0
        self.price = 50
        self.price_gauge = 1.2

    def get_upgrade_cost(self):
        return ((self.price * (self.version+1))**self.price_gauge)

    def buy_effect(self):
        pass

    def buy(self, who):
        if who.money >= self.get_upgrade_cost():
            who.money -= self.get_upgrade_cost()
            base.game.sfx['buy'].play()
            if self.version == 0:
                Message('You bought a copy of {}'.format(self.name))
            else:
                Message('Upgraded {}'.format(self.name))
            self.version += 1
            self.buy_effect()
        else:
            Message('insufficient funds')

    def use(self, who, on, along):
        pass


class Spoof(Warez):
    def __init__(self):
        super().__init__('Spahoof')
        self.price = 30
        self.price_gauge = 1.4

    def buy_effect(self):
        base.game.player.stats.traced[1] = int(base.game.player.stats.traced[1]**1.4)

    def spoof(self, i, who):
        levels = ['guest', 'user', 'admin']
        if who.userlevel == levels[i]:
            Message('You are already at that level.')
            base.game.sfx['min'].play()
        elif levels[i] == 'admin' and self.version <= 3:
            Message("Spahoof needs to be at least at level 3 to spoof as an admin")
            base.game.sfx['sad'].play()
        else:
            if base.game.event: base.game.event.end()
            who.userlevel = levels[i]
            Message('You are now spoofed as {}.'.format(levels[i]))
            base.game.player.tile = base.game.current_map.center.start
            x, y = base.game.current_map.center.start.x, base.game.current_map.center.start.y
            base.game.player.root.set_pos(x, -y, 0)
            base.game.zap('spoofing...')
            base.game.sfx['spoof'].play()
            if i == 1:
                base.game.play_song('c')
            elif i == 2:
                base.game.play_song('d')

    def use(self, who, on, along):
        if who.userlevel != 'owner':
            items = []
            if not who.userlevel == 'guest':
                items.append(('AS GUEST', self.spoof, 0, who))
            if not who.userlevel == 'user':
                items.append(('AS USER', self.spoof, 1, who))
            if not who.userlevel == 'admin':
                items.append(('AS ADMIN', self.spoof, 2, who))
            items.append(('nvm', along))
            Menu(items, title='spoof')
        else:
            base.game.sfx['min'].play()
            Message('No connection found.')


class Scanner(Warez):
    def __init__(self):
        super().__init__('ScanBoy Pro')
        self.price = 40
        self.price_gauge = 1.4

    def use(self, who, on, along):
        if type(on) == Data:
            on.display()
            Message('You scan the data...')
            base.game.sfx['scan'].play()
        elif hasattr(on, 'is_enemy'):
            if self.version*2 > on.version:
                on.display(along)
                base.game.sfx['scan'].play()
            else:
                Message('Unknown security system.')
            Message('You scan the security system...')
        else:
            Message('There is nothing to scan.',callback=along)
            base.game.sfx['min'].play()


class Download(Warez):
    def __init__(self):
        super().__init__('DownloadSavant')
        self.price = 64
        self.price_gauge = 1.4

    def download(self, who, on):
        zsize = int(on.size/(self.version))
        if zsize > who.drive[1]-who.drive[0]:
            Message('It wouldnt fit on your drive.')
            base.game.sfx['min'].play()
        else:
            base.game.sfx['download'].play()
            Message('You download the data...', base.game.event.end)
            who.data.append(on)
            on.tile.data = None
            who.drive[0] += zsize
            for door in on.tile.doors.values():
                for stage in door.np.find_all_texture_stages():
                    door.np.set_texture(stage, loader.load_texture('assets/unset.png'), 1000)
            if on.size > zsize:
                Message('This wouldnt have fit on your drive, but your DownloadSavant version compressed it!')

    def use(self, who, on, along):
        if type(on) == Data:
            self.download(who, on)
            Message('Downloading...')
        else:
            base.game.sfx['min'].play()
            Message('No data found.',callback=along)


class Shred(Warez):
    def __init__(self):
        super().__init__('Shred')
        self.price = 60
        self.price_gauge = 1.3

    def use(self, who, on, along):
        if type(on) == Data:
            base.game.sfx['shred'].play()
            Message('You shred the data...', base.game.event.end)
            on.tile.data = None
            for door in on.tile.doors.values():
                for stage in door.np.find_all_texture_stages():
                    door.np.set_texture(stage, loader.load_texture('assets/unset.png'), 1000)
        elif hasattr(on, 'is_enemy'):
            if on.scanned:
                Message('You shred security...', on.attack(self.version))
                base.game.sfx['shred'].play()
            else:
                base.game.sfx['min'].play()
                Message('Your shred was unable to identify security.', callback=along)
            Message('You try to shred.')
        else:
            base.game.sfx['min'].play()
            Message('There is nothing to shred.', callback=along)
