from panda3d.core import SequenceNode, NodePath, TextureStage


def sheet_uv(node, texture, columns, rows, id):
    w, h = 1/columns, 1/rows
    tile_x, tile_y = int(id%columns), int(id/(columns))
    u, v = (tile_x*w), 1-((tile_y*h)+h)
    node.set_texture(texture)
    node.set_transparency(True)
    node.set_tex_scale(TextureStage.get_default(), w, h)
    node.set_tex_offset(TextureStage.get_default(), (u, v))
    for stage in node.find_all_texture_stages():
        node.set_texture(stage, texture, 1)
        node.set_tex_scale(stage, w, h)
        node.set_tex_offset(stage, (u, v))
    return node

def animated_texture(node, texture, columns, rows, playrate=1):
    sequence = NodePath(SequenceNode('sprite'))
    for i in range(columns*rows):
        sheet_uv(node.copy_to(sequence), texture, columns, rows, i)
    sequence.node().set_frame_rate(5)
    sequence.node().loop(True)
    return sequence
