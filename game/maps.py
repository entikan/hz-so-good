from random import randint, shuffle, uniform
from copy import copy
from panda3d.core import NodePath
from panda3d.core import Vec2
from direct.actor.Actor import Actor
from .shaders import SHADER
from .warez import Data
from .events import ZapFade
from .gui import Message

DIRECTIONS = {'n':Vec2(0,-1),'e':Vec2(1,0),'s':Vec2(0,1),'w':Vec2(-1,0),'c':Vec2(0,-1)}
OPPOSITES = {'n':'s','s':'n','e':'w','w':'e','c':'s'}


class Overseeer():
    def __init__(self):
        self.diff = 1



class Door():
    def __init__(self, np):
        self.np = np
        self.is_open = False
        self.is_locked = False
        self.userlevel = 'guest'

    def close(self,t):
        self.np.play('close')
        self.is_open = False

    def open(self):
        base.game.current_map.visited = True
        self.np.play('open')
        self.is_open = True
        base.task_mgr.do_method_later(0.6,self.close,'close')
        base.sequence_player.finalize()


class Tile():
    def __init__(self, x, y):
        self.x, self.y = x, y
        self.is_start = self.is_end = self.is_dead_end = False
        self.neighbours, self.doors, self.panel = {}, {}, {}
        self.data = None


class Chunk():
    def __init__(self, d='n', size=Vec2(3,3), userlevel='guest', sprite=0):
        self.userlevel = userlevel
        self.root = NodePath('chunk_'+d)
        self.direction = d
        self.tiles = {}
        self.size = size
        self.dead_ends = []
        self.data = []
        self.sprite = sprite

    def get_start_pos(self, d):
        starts = {
            'n':Vec2(self.size.x//2,self.size.y),
            'e':Vec2(0,self.size.y//2),
            's':Vec2(self.size.x//2,0),
            'w':Vec2(self.size.x,self.size.y//2),
            'c':Vec2(self.size.x//2,self.size.y//2),
        }
        return copy(starts[d])

    def set_start(self):
        c = self.curse = self.get_start_pos(self.direction)
        self.stack = [copy(c)]
        self.start = Tile(c.x, c.y)
        self.prev = self.tiles[c.x, c.y] = self.start
        self.tiles[c.x, c.y].is_start = True

    def step(self):
        directions = list('nesw')
        shuffle(directions)
        c = self.curse
        while len(directions) > 0:
            d = directions.pop(0)
            c += DIRECTIONS[d]
            if c.x < 0 or c.x > self.size.x or c.y < 0 or c.y > self.size.y or (c.x, c.y) in self.tiles:
                c -= DIRECTIONS[d]
                continue
            tile = Tile(c.x, c.y)
            self.prev.neighbours[d] = tile
            tile.neighbours[OPPOSITES[d]] = self.prev
            self.prev = tile
            self.tiles[c.x, c.y] = tile
            self.stack.append(copy(c))
            return True

    def room(self):
        self.set_start()
        for y in range(int(self.size.y)+1):
            for x in range(int(self.size.x)+1):
                tile = self.tiles[x,y] = Tile(x, y)
                if y > 0:
                    tile.neighbours['n'] = self.tiles[x,y-1]
                    self.tiles[x,y-1].neighbours['s'] = tile
                if x > 0:
                    tile.neighbours['w'] = self.tiles[x-1,y]
                    self.tiles[x-1,y].neighbours['e'] = tile
        if not self.direction == 'c':
            c = self.curse
            c += DIRECTIONS[self.direction]
            self.start.neighbours[self.direction] = self.tiles[c.x, c.y]
            self.tiles[c.x, c.y].neighbours[OPPOSITES[self.direction]] = self.start
            self.mark_dead_ends()
        else:
            pass
            self.start = self.tiles[self.size.x//2,self.size.y//2]
            self.start.is_start = True
        return self

    def maze(self):
        self.set_start()
        c = self.curse
        while True:
            step = self.step()
            if not step or randint(0,4) == 0:
                if len(self.stack) > 1:
                    self.curse = copy(self.stack.pop())
                    self.prev = self.tiles[self.curse.x, self.curse.y]
                else:
                    break
        self.mark_dead_ends()
        return self

    def mark_dead_ends(self):
        for tile in self.tiles.values():
            if len(tile.neighbours) <= 1 and not tile.is_start and not tile.is_end:
                tile.is_dead_end = True

    def build(self, parts):
        self.texture = loader.load_texture('assets/sets/set_{}.png'.format(self.sprite), minfilter=0, magfilter=0)

        to_flatten = self.root.attach_new_node('flatten')
        for tile in self.tiles.values():
            floor = parts.find('floor').copy_to(to_flatten)
            floor.set_pos(tile.x,-tile.y,0)
            ceil = parts.find('ceiling').copy_to(to_flatten)
            ceil.set_pos(tile.x,-tile.y,uniform(-0.2,0))
            for a, direction in enumerate(('n','e','s','w')):
                if not direction in tile.neighbours:
                    wall = parts.find('wall').copy_to(to_flatten)
                    wall.set_pos(tile.x,-tile.y,0)
                    wall.set_h(-a*90)
                elif tile.is_dead_end or tile.is_start:
                    doorway = parts.find('doorway').copy_to(to_flatten)
                    doorway.set_pos(tile.x,-tile.y,0)
                    doorway.set_h(-a*90)
                    doorway.find('**/door').hide()
                    door = Actor('assets/door.bam')
                    for stage in door.find_all_texture_stages():
                        door.set_texture(stage, self.texture, 1000)
                    door.set_texture(self.texture, 100)
                    door.set_pos(tile.x,-tile.y,0)
                    door.set_h(-a*90)
                    door.reparent_to(self.root)
                    door = Door(door)
                    tile.doors[direction] = door
                    door.userlevel = self.userlevel
                    tile.neighbours[direction].doors[OPPOSITES[direction]] = door
                    door.np.set_shader(SHADER)
                    door.np.set_shader_input('speed', 0.003)
                    if tile.is_dead_end:
                        tile.data = Data(self.userlevel)
                        tile.data.tile = tile
                        self.data.append(tile.data)


        to_flatten.flatten_strong()
        to_flatten.set_shader(SHADER)
        to_flatten.set_shader_input('speed', 0.002)
        for node in to_flatten.find_all_matches('**/*'):
            for stage in node.find_all_texture_stages():
                node.set_texture(stage, self.texture, 1000)



class Map():
    def __init__(self, name='127.0.0.1', difficulty=0):
        self.visited = False
        self.name = name
        self.difficulty = difficulty
        self.root = NodePath('map_'+name)
        self.parts = loader.load_model('assets/parts.bam')
        sprite = difficulty%5
        center = Chunk('c',Vec2(4,4), sprite=sprite).room()
        if difficulty == 0 or difficulty == 1:
            chunks = 'ns'
        elif difficulty == 2 or difficulty == 3:
            chunks = 'ns'
        elif difficulty == 4 or difficulty == 5:
            chunks = 'new'
        else:
            chunks = 'nesw'
        if difficulty < 2:
            levels = ['guest', 'admin']
        elif difficulty < 4:
            levels = ['user', 'user']
        elif difficulty == 4:
            levels = ['guest', 'user', 'admin']
        elif difficulty == 5:
            levels = ['user', 'user', 'admin']
        elif difficulty == 6:
            levels = ['user','user', 'admin', 'admin']
        else:
            levels = ['admin','admin', 'admin', 'admin']
        self.chunks = {}
        for c, chunk in enumerate(chunks):
            self.chunks[chunk]=Chunk(chunk,userlevel=levels[c],sprite=sprite).maze()

        for d in self.chunks:
            pos = center.get_start_pos(OPPOSITES[d])
            c = center.tiles[pos.x, pos.y]
            chunk = self.chunks[d]
            c.neighbours[d] = chunk.start
            chunk.start.neighbours[OPPOSITES[d]] = c
            chunk.build(self.parts)
            chunk.root.reparent_to(self.root)
            #TODO: this is supposed to be a maths but doiiii
            if d == 'n':
                chunk.root.set_pos(1,4,0)
            elif d == 'e':
                chunk.root.set_pos(5,-1,0)
            elif d == 's':
                chunk.root.set_pos(1,-5,0)
            elif d == 'w':
                chunk.root.set_pos(-4,-1,0)

        center.build(self.parts)
        center.root.reparent_to(self.root)
        self.center = center

    def play_song(self):
        base.game.play_song('b')

    def connect(self, player):
        ZapFade('Connecting to '+self.name, self.play_song)
        player.root.reparent_to(self.root)
        player.root.set_pos(self.center.start.x, -self.center.start.y, 0)
        player.tile = self.center.start
        player.stats.userlevel = 'guest'
        player.stats.traced[0] = player.stats.traced[1]
        base.game.current_map = self
        base.game.sfx['connect'].play()
